import time
from locust import HttpUser, task

class QuickstartUser(HttpUser):
    @task
    def hello_world(self):
        self.client.get("/api/numericalintegrationfunction/?lower=0&upper=3.14159")

    host = "https://lab2-4.azurewebsites.net"
