import logging
import math
import azure.functions as func

def abs_sin(x):
    return abs(math.sin(x))

def numerical_integration(lower, upper, N):
    interval_width = (upper - lower) / N
    total_area = 0

    for i in range(N):
        x_i = lower + i * interval_width
        rectangle_area = interval_width * abs_sin(x_i)
        total_area += rectangle_area

    return total_area

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    lower = float(req.params.get('lower'))
    upper = float(req.params.get('upper'))
    intervals = [10, 100, 1000, 10000, 100000, 1000000]
    result_dict = {}

    for N in intervals:
        result = numerical_integration(lower, upper, N)
        result_dict[f'N={N}'] = result

    return func.HttpResponse(f"{str(result_dict)}")
