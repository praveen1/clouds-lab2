import time
from locust import HttpUser, task

class QuickstartUser(HttpUser):
    @task
    def hello_world(self):
        self.client.get("/numericalintegralservice/0/3.14159")

    host = "http://20.74.44.79"
