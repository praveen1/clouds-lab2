import time
from locust import HttpUser, task

class QuickstartUser(HttpUser):
    @task
    def hello_world(self):
        self.client.get("/numericalintegralservice/0/3.14159")

    host = "https://gentle-moss-09b6099b3111495690c53df095f91c14.azurewebsites.net"
