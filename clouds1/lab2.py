from flask import Flask, request, jsonify
import math

app = Flask(__name__)

def abs_sin(x):
    return abs(math.sin(x))

def numerical_integration(lower, upper, N):
    interval_width = (upper - lower) / N
    total_area = 0

    for i in range(N):
        x_i = lower + i * interval_width
        rectangle_area = interval_width * abs_sin(x_i)
        total_area += rectangle_area

    return total_area

@app.route('/numericalintegralservice/<string:lower>/<string:upper>', methods=['GET'])
def numerical_integral_service(lower, upper):
    lower = float(lower)
    upper = float(upper)
    intervals = [10, 100, 1000, 10000, 100000, 1000000]
    result_dict = {}

    for N in intervals:
        result = numerical_integration(lower, upper, N)
        result_dict[f'N={N}'] = result

    return jsonify(result_dict)

if __name__ == '__main__':
    app.run(debug=True)

